# Hackathon 2024 Chemoinformatics

Welcome to the repository for the Hackathon 2024 Chemoinformatics event. This event is organized by a collaboration of experts from various prestigious institutions, focusing on the development and application of chemoinformatics tools and databases.

## Organizers

- **Črtomir Podlipnik** - Faculty of Chemistry and Chemical Technology, University of Ljubljana
- **Gilles Marcou** - Laboratoire de Chémoinformatique, UMR7140 CNRS-Université de Strasbourg
- **Marko Jukič** - Laboratory of Physical Chemistry and Chemical Thermodynamics, UM FKKT
- ... (additional organizers)

## Event Details

- **Start**: 21/06/2024 at 8am
- **End**: 21/06/2024 at 8pm
- **Restitution**: 24/06/2024 at 2pm
- **Location**: Studium, 1 rue Blaise Pascal, 67000 Strasbourg, France

## Hackathon Tasks

Participants will engage in various tasks including but not limited to:

- Composing datasets of compounds with TET activity
- Classifying activities (Covalent/Non-covalent, Allostery, etc.)
- Documenting the source of data
- Standardizing chemical structures

## Software and Databases

Participants will have access to a range of databases and software:

- **Databases**: ChEMBL, PubChem, BindingDB, IUPHAR, SureChem
- **Software**: Chemaxon, KNIME, Python (Rdkit, Indigo), MOE, Schrödinger

## Schedule

The hackathon will follow the schedule below:

- **Lunch Break**: Restaurant le "32"
- **Technical Sessions**: Amphitheater Guy Ourisson, Rooms E.12 and E.30

## Participation

A minimum of 5 persons is required for each task. Participants are encouraged to bring their own laptops, although fully equipped computer rooms are available.

## Licenses

Participants will have access to demo licenses for various software during the event. Please refer to the `Licenses` directory for more details.

## Repository Structure

This repository contains all the necessary files and documentation related to the hackathon:

- `Datasets/` - Compiled datasets of compounds
- `Scripts/` - Code and scripts used in the hackathon
- `Documentation/` - Detailed documentation and bibliographic sources
- `Licenses/` - Information about software licenses

## Contributing

We welcome contributions from all participants. Please submit your pull requests or issues through the GitLab interface.

## Contact

For any inquiries, please contact the organizing committee.

