# Contributing to Hackaton_2024_Chemoinformatics

We welcome contributions from all participants of the Hackathon 2024 Chemoinformatics. If you're looking to contribute, please take a moment to review this document.

## How to Contribute

1. **Fork the Repository**: Start by forking the repository to your own GitHub account.
2. **Clone Locally**: Clone the forked repository to your local machine to start making changes.
3. **Create a Branch**: Create a new branch for your modifications. Use a clear and descriptive name for your branch.
4. **Make Your Changes**: Implement your changes, additions, or fixes.
5. **Commit Your Changes**: Make sure to write clear and detailed commit messages.
6. **Push to Your Fork**: Push your changes to your fork on GitHub.
7. **Submit a Pull Request**: Open a pull request to the main repository. Fill in the provided PR template with the necessary information.
8. **Code Review**: Wait for the code review and respond to any feedback.

## Contribution Guidelines

- **Code Standards**: Follow the coding standards and best practices for the languages and tools used.
- **Testing**: Add tests for new features and ensure that all tests pass.
- **Documentation**: Update the documentation to reflect your changes or additions.
- **Respect**: Treat fellow contributors with respect. Engage constructively in discussions and code reviews.

## Reporting Bugs

- **Use the Issue Tracker**: Report bugs using the GitHub issue tracker for the repository.
- **Describe the Bug**: Provide a detailed description of the bug, including steps to reproduce, expected behavior, and actual behavior.
- **Screenshots**: If applicable, add screenshots to help explain the problem.

## Proposing Enhancements

- **Open an Issue**: Propose enhancements by opening an issue.
- **Detail Your Proposal**: Clearly describe the enhancement, its benefits, and any potential drawbacks.
- **Discuss**: Engage with the community to discuss the proposal and refine it.

## Questions

If you have any questions about contributing, please reach out to the organizing committee or open an issue for discussion.

Thank you for contributing to the Hackaton_2024_Chemoinformatics project!
