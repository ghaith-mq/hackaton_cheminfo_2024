# Hackathon 2024 Chemoinformatics - General Information

## Computer Rooms

- **Room E.30** - 1st floor
- **Room E.12** - 1st floor

### Access to the Rooms

- Keys are available at the welcome desk. Please ask for the bag with keys.
- Visit Room E.12 the day before the event to ensure that the laptops are charging.

### Login Information

- Each computer uses the same authentication credentials. Details can be found in the bag of the room.

### Internet Access

- **OSIRIS**: Unique access account created by J.C. Pont on 13/06.
- **Remote Access**: Access to chemistry computers is available at Unistra Chemistry Remote Access.
- **HPC Cluster**: Access details are provided in the file directory Unistra Seafile.

## Roles and Material

- **Head of Material**: Črtomir Podlipnik
- **Coordination**: Contact Fanny Bonachera or Olga Klimchuk for updated material.
- **Technical Access**: For software and datasets, refer to Joao's Git Joao's GitLab.

## Licenses

- **BiosolveIT**: Almost done.
- **LigandScout**: Done.
- **MOE**: Done.
- **Schrödinger**: Contact Luisa for details.
- **YASARA**: Group license available. Contact Črtomir for more information.

## Schedule

- **Start**: 21/06/2024 at 8 am
- **End**: 21/06/2024 at 8 pm
- **Restitution**: 24/06/2024 at 2 pm

## Location

- **Amphitheater Guy Ourisson**
- **Studium**: 1, rue Blaise Pascal, 67000 Strasbourg, France

## Technical Information

- Each room can accommodate approximately 20 persons.
- The rooms are fully equipped with computers, Wi-Fi, and Visio.
- Participants have access to Unistra library resources.
- Participants are welcome to bring their own laptops.

## Meals and Breaks

- Vouchers for drinks and snacks are available at the cafet' of Studium.
- Lunch will be at Restaurant le "32".

For any further information or assistance, please contact the organizing committee.

# Groups of Students Distributed to Tasks

## Task 1: CMDock
- Poulet
- Ihechu
- Krokhina
- Raffaud
- **Main coordinator:** Marko

## Task 2: Indigo
- Legrand
- Brignoli
- Asghar
- Schjelde
- **Main coordinator:** Fanny

## Task 3: Ichem
- Blanc
- Gilmullin
- Panzeri
- Krüger
- **Main coordinator:** Guillaume

## Task 4: CHA
- Godec
- Lee
- Queiroz Santiago
- Iwan
- **Main coordinator:** Gilles Marcou

## Task 5: Bird Flu
- Juarez Marckwordt
- Milova
- Boldtumur
- van Schendel
- **Main coordinator:** Crtomir

## Task 6: TET - ligands
- Ho
- Davidov
- Afonso Costa
- Hunklinger
- **Main coordinator:** Dragos Horvath

## Task 7: TET - enzymes
- Nguyen
- Mirandela
- Kennedy
- Mqawass
- **Main coordinator:** Dragos Horvath
