---
title: hackathon - conda
author: gbret
date: 2024-06-20
---

# conda

## Install
```sh
mkdir -p ~/hackathon/opt/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/hackathon/opt/miniconda3/miniconda.sh
bash ~/hackathon/opt/miniconda3/miniconda.sh -b -u -p ~/hackathon/opt/miniconda3
rm -rf ~/hackathon/opt/miniconda3/miniconda.sh
```


After installing, either:
1. source profile :
```sh
. ~/hackathon/opt/miniconda3/etc/profile.d/conda.sh
```
2. or initialize miniconda :
```sh
~/hackathon/opt/miniconda3/bin/conda init bash
```


## Create Envs
```sh
conda env create -f hackathon.yml 
conda env create -f sphinx.yml 
```