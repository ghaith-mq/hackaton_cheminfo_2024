---
title: IChem
author: gbret
date: 2024-06-20
keywords:
---

# IChem


## Install
```sh
mkdir -p ~/hackathon/opt
cd ~/hackathon/opt
wget https://gitlab.com/CrtomirP/hackaton_cheminfo_2024/-/raw/main/3.%20MATERIAL/2.%20TUTORIALS/3.1%20ICHEM/IChem.tar.gz
tar -xzf IChem.tar.gz
rm -f IChem.tar.gz

mkdir -p ~/hackathon/data
cd ~/hackathon/data
wget https://gitlab.com/CrtomirP/hackaton_cheminfo_2024/-/raw/main/3.%20MATERIAL/2.%20TUTORIALS/3.1%20ICHEM/IChem_data.tar.gz
tar -xzf IChem_data.tar.gz
rm -f ~/hackathon/data/IChem_data.tar.gz
```

## Configure
```sh
export ICHEM_LIB=~/hackathon/opt/IChem/lib
export ICHEM_LIC=~/hackathon/opt/IChem/IChem.lic
export PATH="${PATH:+${PATH}:}~/hackathon/opt/IChem"
```

## Tutorial
### IFP
```sh
cd ~/hackathon/data/IChem/IFP 
rm -rf output
mkdir output
IChem IFP site.mol2 ligand.mol2 >./output/ligand.ifp
IChem IFP site.mol2 docked.mol2 ligand.mol2 >output/docked_Tc.ifp
```

### INTS
```sh
cd ~/hackathon/data/IChem/TIFP
rm -rf output
mkdir output
cd output
IChem -logf 2rh1_ints.txt -type MER ints ../site.mol2 ../ligand.mol2 &> /dev/null 
IChem -fgps STD ints ../site.mol2 ../ligand.mol2 2rh1_full.fgp &> /dev/null 
IChem --small -fgps STD ints ../site.mol2 ../ligand.mol2  2rh1_small.fgp &> /dev/null 
/bin/rm 1 2
```

### GRIM
```sh
cd ~/hackathon/data/IChem/GRIM
rm -rf output
mkdir output
cd output
IChem -sim 1 -rn 2rh1 -cn 4amj grim ../2rh1_prot.mol2 ../2rh1_lig.mol2 ../4amj_prot.mol2 ../4amj_lig.mol2 &> /dev/null 
cd ..
cd screen 
rm -rf output
mkdir output
cd output
IChem -sim 1 grim ../site.mol2 ../ligand.mol2 ../docked.mol2 &> /dev/null 
```

### REALIGN
```sh
cd ~/hackathon/data/IChem/REALIGN
rm -rf output
mkdir output
cd output
IChem --wMob realign ../GRIM_ints.mol2 ../4amj_INTS_M.mol2  ../4amj_lig.mol2 ../4amj_prot.mol2
sed -n '4,128p' rot_4amj_lig.mol2 >toto
```

### VOLSITE
```sh
cd ~/hackathon/data/IChem/VOLSITE/unrestricted
rm -rf output
mkdir output
cd output
IChem --desc volsite ../protein.mol2 &> /dev/null 
cd ../../ligand
rm -rf output
mkdir output
cd output
IChem volsite ../protein.mol2 ../ligand.mol2 &> /dev/null 
```


### PDBCONV
```sh
cd ~/hackathon/data/IChem/PDBCONV
rm -rf output
mkdir output
IChem pdbconv 2RH1.pdb output 2rh1 1> ./output/pdbconv.out 2> ./output/pdbconv.err
```

### BSA
```sh
cd ~/hackathon/data/IChem/BSA
rm -rf output
mkdir output
cd output
IChem utils bsa ../protein.mol2 ../ligand.mol2 > ligand.bsa
```


### SIMS
```sh
cd ~/hackathon/data/IChem/SIMILARITY 
rm -rf output
mkdir output
cd output
IChem sims ../FP1.txt ../FP2.txt >sim.txt 
```

