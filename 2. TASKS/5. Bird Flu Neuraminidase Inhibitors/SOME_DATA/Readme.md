### About data

The PDB entries 2HTW, 2QWK, 3CL0, 4MNP, and 4HP0 provide crucial structural insights into various proteins and complexes. Notably, several of these structures target proteins related to bird flu, specifically influenza virus neuraminidase. These entries reveal detailed interactions between viral proteins and inhibitors, aiding in the understanding of resistance mechanisms and the development of antiviral drugs. They collectively enhance our knowledge of viral pathogenesis and potential therapeutic strategies.

### PDB Code: 2HTW

This PDB entry presents the crystal structure of N4 neuraminidase from Influenza A virus in complex with the inhibitor DANA, resolved at 3.50 Å. Neuraminidase facilitates viral release from infected cells. The study by Russell et al., published in Nature, highlights structural differences between group-1 and group-2 neuraminidases, revealing a unique cavity in group-1 that can be targeted for new antiviral drug designs. DOI: [10.2210/pdb2HTW/pdb](https://doi.org/10.2210/pdb2HTW/pdb).

### PDB Code: 3CL0

This entry details the crystal structure of the H274Y mutant N1 neuraminidase from the Influenza A virus (A/Viet Nam/1203/2004(H5N1)), complexed with the antiviral drug oseltamivir. It reveals insights into the resistance mechanism against oseltamivir. The structure was determined using X-ray diffraction at a 2.20 Å resolution. Deposited on March 18, 2008, and released on May 20, 2008, by Collins et al., this study highlights the necessity of diverse antiviral stockpiles due to emerging drug resistance. DOI: [10.1038/nature06956].

### PDB Code: 2QWK

This entry features the X-ray structure of a complex between the wild-type N9 influenza virus neuraminidase and the inhibitor GS4071. The structure, determined at 1.80 Å resolution, provides insights into drug design and resistance mechanisms. Deposited on April 7, 1998, and released on November 11, 1998, by Varghese et al. [DOI: 10.1016/s0969-2126(98)00075-6](https://doi.org/10.1016/s0969-2126(98)00075-6) [PDB DOI: 10.2210/pdb2QWK/pdb](https://doi.org/10.2210/pdb2QWK/pdb)

### PDB Code: 4MNP

The structure of the sialic acid-binding protein from *Fusobacterium nucleatum* subsp. *nucleatum* ATCC 25586 reveals a conserved binding site crucial for sugar binding. Determined by X-ray diffraction at 2.50 Å resolution, it aids in understanding bacterial sialic acid transport. Deposited on September 11, 2013, and released on July 9, 2014. [PDB DOI: 10.2210/pdb4MNP/pdb](https://doi.org/10.2210/pdb4MNP/pdb)

### PDB Code: 4HP0
The crystal structure of hen egg white lysozyme in complex with GN3-M, determined by X-ray diffraction at 1.19 Å resolution, reveals binding at subsites -4 to -1. This study, deposited on October 23, 2012, and released on January 16, 2013, by Umemoto et al., supports the covalent glycosyl-enzyme intermediate in lysozyme's catalytic mechanism, enhancing our understanding of enzyme inhibition. [DOI: 10.1074/jbc.M112.439281](https://doi.org/10.1074/jbc.M112.439281).[PDB DOI: 10.2210/pdb4HP0/pdb](https://doi.org/10.2210/pdb4HP0/pdb)