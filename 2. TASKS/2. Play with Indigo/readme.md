# Indigo Tasks - Hackathon 2024 Chemoinformatics

## Group 2: Indigo

- Legrand
- Brignoli
- Asghar
- Schjelde

Main coordinator: Fanny

## Introduction

This section of the repository is dedicated to tasks related to the Indigo cheminformatics toolkit. Participants are expected to engage in a variety of tasks from setting up a compilation platform to proposing new functionalities and improving documentation.

## Tasks Overview

- **Prepare a Compilation Platform**: Set up an environment for compiling Indigo on Windows using Visual Studio and ensure access to a remote Linux compilation platform.
- **Propose Functionalities**: Suggest new features that can enhance the Indigo toolkit.
- **Reference Bugs**: Identify and document any existing bugs within Indigo.
- **Propose Patches for Bugs**: Develop and submit patches for identified bugs.
- **Complete the Documentation**: Ensure that the documentation is comprehensive and up-to-date.
- **Propose Tutorials**: Create tutorials that help new users understand how to use Indigo.
- **Submit the Contribution to Community**: Share your work with the broader cheminformatics community.

## Examples

- **Compilation on Windows**: Address issues with libraries not supported by Windows.
- **Chimiothèque Nationale Molecules**: Check and replace the first 10k molecules if needed.
- **SDF Comparison**: Compare SDF files generated with Indigo and ChemAxon, especially focusing on pKa values.
- **Sanitization of Structures**: Implement structure sanitization checks including valence checking and aromatization following Hückel rules.

## Debugging and Functionalities

- **Preferred Language**: C++
- **Development and Compilation Platform**: Windows - Visual Studio, with access to remote Linux platforms.
- **Debugging**: Address and fix any issues that arise during the development process.

## Testing

- **Unit Test**: Develop and run unit tests to ensure each part of the software works correctly.
- **Convergence Test**: Prepare targets with known output and compare them to reference results on a local install (minimum of 1 person required).

## Documentation

- **CLI Documentation**: Document the command-line interface usage, including prerequisites, installation, running the software, and interpreting the results (minimum of 1 person required).

## Contribution Guidelines

Please refer to the `CONTRIBUTING.md` file for guidelines on how to contribute to this project.

## Support

For any support related to the Indigo tasks, please contact the organizing committee or raise an issue in this repository.